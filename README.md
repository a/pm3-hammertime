# pm3-hammertime

Small lazy scripts to stress test different card techs on proxmark3.

Drop into `Proxmark3/client/luascripts`, run with `script run filename` (do `script run filename -h` to see help options).

We've previously utilized these scripts to put in millions of writes to each block on an genuine NTAG215 chip with minimal issues.

